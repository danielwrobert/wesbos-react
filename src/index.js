import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import App from './components/App';
import StorePicker from './components/StorePicker';
import NotFound from './components/NotFound';
import './css/style.css';

const Root = () => {
	return (
		<BrowserRouter>
			<div>
				<Switch>
					<Route exact path="/" component={ StorePicker } />
					<Route path="/store/:storeId" component={ App } />
					<Route component={ NotFound } />
				</Switch>
			</div>
		</BrowserRouter>
	)
};
 
render( <Root />, document.getElementById( 'root' ) );