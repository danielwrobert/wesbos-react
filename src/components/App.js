/** @format */

import React from 'react';
import PropTypes from 'prop-types';
import Header from './Header';
import Order from './Order';
import Inventory from './Inventory';
import Fish from './Fish';
import sampleFishes from '../sample-fishes';
import base from '../base';

class App extends React.Component {
	constructor() {
		super();

		this.addFish = this.addFish.bind( this ); // Needed to bind method to this class ( can also use Property Initializer Syntax on function def)
		this.loadSamples = this.loadSamples.bind( this );
		this.addToOrder = this.addToOrder.bind( this );

		// Initial state (same as getInitialState with React.CreateClass - old approach)
		this.state = {
			fishes: {},
			order: {},
		};
	}

	componentWillMount() {
		// This runs right before the <App> is rendered
		const { params } = this.props.match;

		this.ref = base.syncState( `${ params.storeId }/fishes`, {
			context: this,
			state: 'fishes',
		} );

		// Check if there is any order in localstorage
		const localStorageRef = localStorage.getItem( `order-${ params.storeId }` );

		if ( localStorageRef ) {
			// update our App component's order state
			this.setState( {
				order: JSON.parse( localStorageRef ), // JSON.parse will return the strink stored in Local Storage back into an object to be used for State.
			} );
		}
	}

	componentWillUnmount() {
		base.removeBinding( this.ref );
	}

	componentWillUpdate( nextProps, nextState ) {
		const { params } = this.props.match;

		localStorage.setItem( `order-${ params.storeId }`, JSON.stringify( nextState.order ) ); // need to stringify because you can not store an object inside of Local Storage.
	}

	addFish( fish ) {
		// Update state
		// What you do NOT want to do (modify state directly):
		// this.state.fishes.fish1 = fish

		// What you DO want to do:
		// 1. copy existing state to fishes variable
		const fishes = { ...this.state.fishes };
		// 2. set timestamp for unique key
		const timestamp = Date.now(); // will give current millisecond value since Jan of 1970
		// 2. add in new fish - using timestamp
		fishes[ `fish-${ timestamp }` ] = fish;

		// Set state
		// this.setState( { fishes: fishes } );
		this.setState( { fishes } ); // the ES6 way - when properties and values are the same.
	}

	updateFish = ( key, updatedFish ) => {
		const fishes = { ...this.state.fishes };
		fishes[ key ] = updatedFish;
		this.setState( { fishes } );
	};

	removeFish = key => {
		const fishes = { ...this.state.fishes };
		fishes[ key ] = null; // Need to set to null, instead of using `delete` because of how Firebase works with the data.
		this.setState( { fishes } );
	};

	loadSamples() {
		this.setState( {
			fishes: sampleFishes,
		} );
	}

	addToOrder( key ) {
		// take a copy of our state
		const order = { ...this.state.order };

		// update or add the new number of fish ordered
		order[ key ] = order[ key ] + 1 || 1;

		// update our state
		this.setState( { order } );
	}

	removeFromOrder = key => {
		const order = { ...this.state.order };
		delete order[ key ]; // In this case, we can use `delete` (unlike in `removeFish`) because we're not syncing order state with Firebase
		this.setState( { order } );
	};

	render() {
		return (
			<div className="catch-of-the-day">
				<div className="menu">
					<Header tagline="Fresh Seafood Market" />
					<ul className="list-of-fishes">
						{ Object.keys( this.state.fishes ).map( key => (
							<Fish
								key={ key }
								index={ key }
								details={ this.state.fishes[ key ] }
								addToOrder={ this.addToOrder }
							/>
						) ) }
					</ul>
				</div>
				<Order
					fishes={ this.state.fishes }
					order={ this.state.order }
					params={ this.props.match.params }
					removeFromOrder={ this.removeFromOrder }
				/>
				<Inventory
					fishes={ this.state.fishes }
					loadSamples={ this.loadSamples }
					addFish={ this.addFish }
					updateFish={ this.updateFish }
					removeFish={ this.removeFish }
					storeId={ this.props.match.params.storeId }
				/>
			</div>
		);
	}
}

App.propTypes = {
	match: PropTypes.object.isRequired,
};

export default App;
