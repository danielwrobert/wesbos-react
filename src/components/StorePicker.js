/** @format */

import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { getFunName } from '../helpers';

class StorePicker extends React.Component {
	// constructor() {
	// 	super();

	// 	this.goToStore = this.goToStore.bind( this );
	// }

	goToStore( event ) {
		const { history } = this.props;

		event.preventDefault();
		console.log( 'You changed the URL!' );
		// First, grab the text from the box
		const storeId = this.storeInput.value; // If you do not bind the goToStore method to the component, `this` will be `null` - In this example, it is bound via an arrow fn when called in the `onSubmit` action of the form below.
		console.log( `Going to ${ storeId }` );
		// Second, transition from '/' to '/store/:storeId'
		// this.context.router.history.push( `/store/${ storeId }` );
		history.push( `/store/${ storeId }` );
	}

	render() {
		return (
			<form className="store-selector" onSubmit={ e => this.goToStore( e ) }>
				<h2>Please Enter A Store</h2>
				{ /* When the input below is rendered to the page, it will place a reference called storeInput on the class itself. */ }
				<input
					type="text"
					required
					placeholder="Store Name"
					defaultValue={ getFunName() }
					ref={ input => {
						this.storeInput = input;
					} }
				/>
				<button type="submit">Visit Store →</button>
			</form>
		);
	}
}

StorePicker.propTypes = {
	history: PropTypes.object.isRequired,
};

// StorePicker.contextTypes = {
// 	router: PropTypes.object
// }
const StorePickerWithRouter = withRouter( StorePicker );

export default StorePickerWithRouter;
