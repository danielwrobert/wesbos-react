/** @format */

import React from 'react';
import PropTypes from 'prop-types';

const Header = props => {
	console.log( this ); // this no longer referrs to the component, in a Stateless Functional Component context.
	return (
		<header className="top">
			<h1>
				Catch
				<span className="ofthe">
					<span className="of">of</span>
					<span className="the">the</span>
				</span>
				Day
			</h1>
			<h3 className="tagline">
				<span>{ props.tagline }</span>
			</h3>
		</header>
	);
};

Header.propTypes = {
	tagline: PropTypes.string.isRequired,
};

export default Header;
