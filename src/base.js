import Rebase from 're-base';
import firebase from 'firebase';

const firebaseApp = firebase.initializeApp( {
	apiKey: 'AIzaSyAcPMk8xHqHPqPJtsMCtU9y5xDeN_cAMrA',
	authDomain: 'catch-of-the-day-dwr.firebaseapp.com',
	databaseURL: 'https://catch-of-the-day-dwr.firebaseio.com'
} );

const base = Rebase.createClass( firebaseApp.database() );

export { firebaseApp };

export default base;